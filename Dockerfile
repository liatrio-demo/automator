FROM python:slim

MAINTAINER Neill Shazly (nshazly@gmail.com)

ENV PORT=3000

RUN apt-get update

COPY /app /app
WORKDIR /app
RUN pip install -r requirements.txt
ENTRYPOINT ["python"]
CMD ["server.py"]