# Automator

This repository contains a sample python flask application with 2 endpoints:

- / - returns a timestamp and message
- /healthz - returns an 200 http code when the server is running

## Run locally

Runt he command `python server.py`

## Build/Deploy Strategy

Deployments are handled by ArgoCD.

- main - this branch is the development branch, ArgoCD will watch and automatically deploy the app to the `dev` namespace in the cluster
- release + tags - these are used to mark production releases
    1. merge/cherry-pick changes from the main branch into the release branch
    2. then tag the release commit using semver (ie. vx.x.x)
    3. Update ArgoCD to point to the new tag

Production releases implement the ArgoCD commit pinning strategy. ArgoCD will monitor a specific tag until it is changed to ensure Production releases remain stable.

The main branch will deploy using the 'latest' tag, ArgoCD is configured to recreate all resource on each deploy in Development.

