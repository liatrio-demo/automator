from flask import Flask
import json, time, os
from prometheus_client import Counter, start_http_server
from werkzeug.middleware.dispatcher import DispatcherMiddleware
from prometheus_client import make_wsgi_app

global COUNTER
PORT = os.getenv('PORT', 80)
COUNTER = Counter('number_of_calls', "Call Counter")

app = Flask(__name__)

# Add prometheus wsgi middleware to route /metrics requests
app.wsgi_app = DispatcherMiddleware(app.wsgi_app, {
    '/metrics': make_wsgi_app()
})

@app.route('/')
def index():
    """
    {   
      “message”: “Automate all the things!”, 
      “timestamp”: 1529729125 
    } 
    """
    global COUNTER
    COUNTER.inc() # increment by 1

    try:
        # time returns a float
        timestamp = int(time.time())
        msg = {"message": "Automate all the things!", "timestamp": timestamp}
    except e:
        print("An error occurred:", e.message)
    
    return json.dumps(msg, indent=2)

@app.route('/healthz')
def healthz():
    return 'healthy', 200

start_http_server(8000) # metrics server
app.run(host='0.0.0.0', port=PORT)